import { Injectable, Injector } from '@angular/core';
import { catchError, finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { DataService } from '../../shared/services/data.service';
import { LoaderService } from '../../shared/services/loader.service';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class TokenInterceptorService implements HttpInterceptor {
  constructor(
    private injector: Injector,
    public loaderService: LoaderService
  ) {}
  intercept(req: any, next: any) {
    this.loaderService.isLoading.next(true);

    let dataService = this.injector.get(DataService);
    let tokeniezdRek = req.clone({
      setHeaders: {
        Authorization: `Bearer ${dataService.getToken()}`,
      },
    });
    return next.handle(tokeniezdRek).pipe(
      finalize(() => {
        this.loaderService.isLoading.next(false);
      })
    );
  }
}
