import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {navItems} from "../../nav";

@Component({
  selector: 'app-default-layout',
  templateUrl: './default-layout.component.html',
  styleUrls: ["../../../../node_modules/simple-line-icons/css/simple-line-icons.css",'./default-layout.component.css']
})
export class DefaultLayoutComponent implements OnInit {
  title:any
  private lastNav:any
  navList:any
  constructor(
    private router: Router,
    ) { }

  ngOnInit(): void {
    // @ts-ignore
    this.navList=JSON.parse(localStorage.getItem('navItems'))
    // @ts-ignore
    this.title=localStorage.getItem('title')
    this.lastNav=localStorage.getItem('lastNav')
  }
  navTo(item:any) {
    this.navList[this.lastNav].active=''
    this.lastNav=item
    this.navList[item].active+='activeNav'
    this.router.navigate([this.navList[item].url]);
    this.title=this.navList[item].name
    localStorage.setItem('title',this.navList[item].name)
    localStorage.setItem('navItems',JSON.stringify(this.navList))
    localStorage.setItem('lastNav',item)
  }

  logOut() {
    this.router.navigate(['login']);
  }
}
