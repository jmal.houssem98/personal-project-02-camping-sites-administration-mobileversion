import { Component, OnChanges, OnInit } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnChanges, OnInit {
  title = 'Huttoloc_Mobile';

  constructor(private swUpdate: SwUpdate) {
    /*swUpdate.available.subscribe((event) => {
      swUpdate.activateUpdate().then(() => document.location.reload());
      //this.update = true;
    });*/
  }

  ngOnInit() {
    this.reloadCache();
  }

  reloadCache() {
    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(() => {
        if (confirm('New version available! would you like to update?')) {
          window.location.reload();
        }
      });
    }
  }
  ngOnChanges() {}
}
