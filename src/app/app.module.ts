import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

//my imports
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { DataService } from './shared/services/data.service';
import { LoginComponent } from './views/login/login.component';
import { HomeComponent } from './views/home/home.component';
import { DefaultLayoutComponent } from './containers/default-layout/default-layout.component';
import { InitialisationComponent } from './views/initialisation/initialisation.component';
import { MiseEnRebutComponent } from './views/mise-en-rebut/mise-en-rebut.component';
import { ReportingComponent } from './views/reporting/reporting.component';
import { FinDeSaisonComponent } from './views/fin-de-saison/fin-de-saison.component';
import { DebutDeSaisonComponent } from './views/debut-de-saison/debut-de-saison.component';
import {
  ANIMATION_MODULE_TYPE,
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';

import { AuthGuard } from './core/authentication/auth.guard';
import { TokenInterceptorService } from './core/interceptors/token-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { WebcamModule } from 'ngx-webcam';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    DefaultLayoutComponent,
    InitialisationComponent,
    MiseEnRebutComponent,
    ReportingComponent,
    FinDeSaisonComponent,
    DebutDeSaisonComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000',
    }),
    NoopAnimationsModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    ZXingScannerModule,
    MatSelectModule,
    MatDialogModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    WebcamModule,
  ],
  providers: [
    { provide: ANIMATION_MODULE_TYPE, useValue: 'BrowserAnimations' },
    AuthGuard,
    DataService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
