import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import apiUrl from '../config';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  readonly APIUrl = apiUrl;
  constructor(private http: HttpClient) {}

  login(username: string, password: string) {
    return this.http.post(this.APIUrl + '/login', { username, password });
  }
  getToken() {
    return localStorage.getItem('token');
  }

  loggedIn() {
    return !!(localStorage.getItem('token') && localStorage.getItem('user'));
  }

  // @ts-ignore
  getEntity(url: string, filters) {
    url += '?';
    // @ts-ignore
    filters.forEach((filter) => {
      let key = Object.keys(filter)[0];
      url += '&' + key + '=' + filter[key];
    });
    return this.http.get(this.APIUrl + url);
  }
  updateEntity(url: string, id: string, updatedValue: any) {
    url += '/';
    return this.http.patch(this.APIUrl + url + id, updatedValue);
  }

  addEntity(url: string, newValue: any) {
    return this.http.post(this.APIUrl + url, newValue);
  }
  getAccommodation(url: string) {
    url += '/';
    return this.http.get('http://127.0.0.1:8000' + url);
  }
  patchEntity(url: string, id: string, patchValue: any) {
    url += '/';
    return this.http.patch(this.APIUrl + url + id, patchValue);
  }
}

//  https://127.0.0.1:8000/api/sites?page=1&itemsPerPage=10
