import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mise-en-rebut',
  templateUrl: './mise-en-rebut.component.html',
  styleUrls: [
    '../../../../node_modules/simple-line-icons/css/simple-line-icons.css',
    './mise-en-rebut.component.css',
  ],
})
export class MiseEnRebutComponent implements OnInit {
  elements: any;
  accommodation: any;
  accommodationId = '';
  accommodationName = '';
  showAlert = false;
  loading = false;
  step = 1;
  scanResult: any = '';
  enable = false;
  devices: any;
  errorCam = false;
  errorCode = false;
  exist = false;
  selectedOptions: any = [];
  constructor(private share: DataService, private router: Router) {}

  ngOnInit(): void {}
  scanQrCode() {
    this.enable = true;
  }
  onCodeResult(result: string) {
    this.scanResult = result;
    this.enable = false;
  }

  sdevices(x: any) {
    console.log('dddd', x);
    this.devices = x;
  }

  notFoundCam() {
    console.log('not found cam');
    this.errorCam = true;
  }

  closeCam() {
    this.enable = false;
    this.errorCam = false;
  }
  confirmCode() {
    this.errorCode = false;
    this.loading = true;
    this.share
      .getEntity('/elements', [{ recognition_code: this.scanResult }])
      .subscribe((res) => {
        this.loading = false;
        // @ts-ignore
        if (res['hydra:member'].length > 0) {
          // @ts-ignore
          res['hydra:member'].map((item) => {
            if (item.recognition_code === this.scanResult) {
              this.loading = false;
              //console.log(item);
              this.accommodationName = item.accommodation.leasing;
              this.accommodationId = item.accommodation.id;
            } else {
              this.errorCode = true;
            }
          });
        } else {
          this.errorCode = true;
        }
      });
  }

  getElements() {
    this.loading = true;
    this.share
      .getEntity(
        '/elements?&state%5B%5D=1&state%5B%5D=2&state%5B%5D=4&accommodation=' +
          this.accommodationId,
        [{ accommodation: this.accommodationId }]
      )
      .subscribe((res) => {
        // @ts-ignore
        this.elements = res['hydra:member'];
        // @ts-ignore
        res['hydra:member'].map((item) => {
          this.share
            .updateEntity('/elements', item.id, {
              state: '/api/element_states/4',
            })
            .subscribe(
              (res) => {
                this.loading = false;
              },
              (error) => {
                console.log(error);
              }
            );
        });
        this.loading = false;
        this.accommodationName = '';
        this.step = 2;
      });
  }

  onNgModelChange($event: any) {
    //console.log('event', $event);
    //console.log(this.selectedOptions);
  }

  newElementState = {
    element: '/api/elements/',
    createdAt: '2022-05-21',
    updatedAt: '2022-05-21',
    user: '/api/users/1',
    newState: '',
  };
  userId: any;

  terminer() {
    this.userId = localStorage.getItem('userId');
    //console.log('user', this.userId);
    this.newElementState.element = '/api/elements/';
    this.newElementState.createdAt = '';
    this.newElementState.newState = '';
    this.newElementState.user = '/api/users/' + String(this.userId);

    let now = new Date();
    console.log(now.toISOString());
    this.selectedOptions.map((element: string) => {
      this.loading = true;
      this.share
        .updateEntity('/elements', element, { state: '/api/element_states/3' })
        .subscribe(
          (res) => {
            this.loading = false;
            this.newElementState.element += String(element);
            this.newElementState.createdAt = now.toISOString();
            this.newElementState.newState = '/api/element_states/3';
            this.share
              .addEntity('/element_histories', this.newElementState)
              .subscribe((res) => {});

            this.newElementState.element = '/api/elements/';
          },
          (error) => {
            console.log(error);
          }
        );
    });
    this.elements = [];
    this.accommodationId = '';
    this.accommodationName = '';
    this.showAlert = false;
    this.loading = false;
    this.step = 1;
    this.scanResult = '';
    this.enable = false;
    this.errorCam = false;
    this.exist = false;
    this.selectedOptions = [];
  }
}
