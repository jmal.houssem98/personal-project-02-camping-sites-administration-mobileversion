import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MiseEnRebutComponent } from './mise-en-rebut.component';

describe('MiseEnRebutComponent', () => {
  let component: MiseEnRebutComponent;
  let fixture: ComponentFixture<MiseEnRebutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MiseEnRebutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MiseEnRebutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
