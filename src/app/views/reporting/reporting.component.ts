import { Component, HostListener, OnInit } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reporting',
  templateUrl: './reporting.component.html',
  styleUrls: ['./reporting.component.css'],
})
export class ReportingComponent implements OnInit {
  constructor(private share: DataService, private router: Router) {}
  liste = [
    'Complètement initialisés',
    'Partiellement initialisés',
    'Non initialisés',
    'Liste des éléments',
  ];
  ngOnInit(): void {
    this.getAccomodations();
    this.sc = 0;
  }
  sc = 0;
  @HostListener('window:scroll', ['$event'])
  scrollHandler(event: any) {
    //console.log('Scroll Event');
    let tracker = event.target;
    let limit = tracker.scrollHeight - tracker.clientHeight;
    if (event.target.scrollTop === limit) {
      this.sc++;
      this.isLoading = true;
      if (this.sc == 1) {
        this.delay(550).then((any) => {
          this.isLoading = false;
          this.displayedVersions = this.Versions.slice(0, 8);
        });
      } else if (this.sc == 2) {
        this.delay(550).then((any) => {
          this.isLoading = false;
          this.displayedVersions = this.Versions.slice(0, 12);
        });
      } else if (this.sc == 3) {
        this.delay(550).then((any) => {
          this.isLoading = false;
          this.displayedVersions = this.Versions.slice(0, 16);
        });
      } else {
        this.isLoading = false;
        this.displayedVersions = this.Versions;
      }
    }
  }

  resultReady1 = false;
  resultReady2 = false;

  step = 0;
  listReady = false;
  isLoading = false;
  Versions: any;
  elementNumber = 0;
  exist: any;
  accommodationAll = [''];
  accommodationId: any;
  accommodationPartiallyInitialized = [''];
  accommodationTotalyInitialized = [''];

  getAccomodations() {
    this.listReady = false;
    this.resultReady1 = false;
    this.resultReady2 = false;
    this.accommodationAll = [];
    this.accommodationPartiallyInitialized = [];
    this.accommodationTotalyInitialized = [];

    this.share.getEntity('/elements', []).subscribe(
      (result) => {
        // @ts-ignore
        result['hydra:member'].map((item) => {
          this.accommodationId = item.accommodation['id'];
          if (this.accommodationAll.indexOf(this.accommodationId) === -1) {
            this.accommodationAll.push(this.accommodationId);
          }
        });
        this.resultReady1 = true;
      },
      (error) => {
        if (error.status == 401) {
          localStorage.clear();
          this.router.navigate(['/login']);
        }
        console.log(error);
      }
    );
    this.share.getEntity('/elements', [{ recognition_code: '' }]).subscribe(
      (result) => {
        // @ts-ignore
        result['hydra:member'].map((item) => {
          this.accommodationId = item.accommodation['id'];
          if (
            this.accommodationPartiallyInitialized.indexOf(
              this.accommodationId
            ) === -1
          ) {
            this.accommodationPartiallyInitialized.push(this.accommodationId);
          }
        });
        this.resultReady2 = true;
      },
      (error) => {
        if (error.status == 401) {
          localStorage.clear();
          this.router.navigate(['/login']);
        }
        console.log(error);
      }
    );
    //api call end here

    this.delay(5000).then((any) => {
      if (this.resultReady1 && this.resultReady2) {
        for (const value of this.accommodationAll) {
          if (this.accommodationPartiallyInitialized.indexOf(value) === -1) {
            this.accommodationTotalyInitialized.push(value);
          }
        }
        //console.log('result ready !');
        this.listReady = true;
      } else {
        //console.log('must wait more --> Itératioon 2');
        this.delay(5000).then((any) => {
          if (this.resultReady1 && this.resultReady2) {
            for (const value of this.accommodationAll) {
              if (
                this.accommodationPartiallyInitialized.indexOf(value) === -1
              ) {
                this.accommodationTotalyInitialized.push(value);
              }
            }
          }
          //console.log('result ready 2!');
          this.listReady = true;
        });
      }
    });
  }
  initNumber = 0;
  partialNumber = 0;
  elementVersion: any;
  elemenStates: any;
  getTotalyInitialized() {
    this.elementVersion = [];
    this.elemenStates = [];
    this.initNumber = 0;
    this.isLoading = true;
    this.delay(5000).then((any) => {
      this.initNumber = this.accommodationTotalyInitialized.length;
      this.step = 1;
      for (const value of this.accommodationTotalyInitialized) {
        this.share
          .getEntity('/accommodations', [{ id: String(value) }])
          .subscribe(
            (result) => {
              this.elementVersion.push(
                // @ts-ignore
                String(result['hydra:member'][0]['version']['name'])
              );
              this.elemenStates.push(
                String(
                  // @ts-ignore
                  result['hydra:member'][0]['state']['name']
                )
              );
            },
            (error) => {
              if (error.status == 401) {
                localStorage.clear();
                this.router.navigate(['/login']);
              }
              console.log(error);
            }
          );
      }
      this.isLoading = false;
    });
  }

  getPartiallyInitialized() {
    this.elementVersion = [];
    this.elemenStates = [];
    this.isLoading = true;
    this.partialNumber = 0;

    this.delay(2500).then((any) => {
      if (!this.listReady) {
        console.log('List not ready yet !');
        this.delay(3500).then((any) => {
          this.partialNumber = this.accommodationPartiallyInitialized.length;
          this.step = 2;
          for (const value of this.accommodationPartiallyInitialized) {
            this.share
              .getEntity('/accommodations', [{ id: String(value) }])
              .subscribe(
                (result) => {
                  this.elementVersion.push(
                    // @ts-ignore
                    String(result['hydra:member'][0]['version']['name'])
                  );
                  this.elemenStates.push(
                    String(
                      // @ts-ignore
                      result['hydra:member'][0]['state']['name']
                    )
                  );
                },
                (error) => {
                  if (error.status == 401) {
                    localStorage.clear();
                    this.router.navigate(['/login']);
                  }
                  console.log(error);
                }
              );
          }
          this.isLoading = false;
        });
      } else {
        //logic here then past above
        this.partialNumber = this.accommodationPartiallyInitialized.length;
        this.step = 2;
        for (const value of this.accommodationPartiallyInitialized) {
          this.share
            .getEntity('/accommodations', [{ id: String(value) }])
            .subscribe(
              (result) => {
                this.elementVersion.push(
                  // @ts-ignore
                  String(result['hydra:member'][0]['version']['name'])
                );
                this.elemenStates.push(
                  String(
                    // @ts-ignore
                    result['hydra:member'][0]['state']['name']
                  )
                );
              },
              (error) => {
                if (error.status == 401) {
                  localStorage.clear();
                  this.router.navigate(['/login']);
                }
                console.log(error);
              }
            );
        }
        this.isLoading = false;
      }
    });
  }

  //old
  displayedVersions = [];
  getVersions() {
    this.isLoading = true;
    this.exist = [
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
    ];
    this.share.getEntity('/versions', []).subscribe(
      (res) => {
        this.step = 3;
        // @ts-ignore
        this.Versions = res['hydra:member'];
        this.displayedVersions = this.Versions.slice(0, 4);
        // @ts-ignore
        res['hydra:member'].map((item) => {
          this.elementNumber = item.element_number;
        });
        for (let i = 0; i <= this.elementNumber; i++) {
          this.exist[i] = true;
        }
        this.isLoading = false;
      },
      (error) => {
        if (error.status == 401) {
          localStorage.clear();
          this.isLoading = false;
          this.router.navigate(['/login']);
        }
        console.log(error);
        this.isLoading = false;
      }
    );
  }
  async delay(ms: number) {
    await new Promise<void>((resolve) => setTimeout(() => resolve(), ms)).then(
      () => console.log('fired')
    );
  }
}
