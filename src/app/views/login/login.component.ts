import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoaderService } from 'src/app/shared/services/loader.service';
import { DataService } from '../../shared/services/data.service';
import { navItems } from '../../nav';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  public formGroup: FormGroup;

  constructor(
    private service: DataService,
    private router: Router,
    public loaderService: LoaderService
  ) {
    this.formGroup = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }
  token: any;
  res: any;
  error: string = '';
  showForm = true;

  ngOnInit(): void {
    localStorage.clear();
    this.showForm = true;
    this.initForm();
  }

  initForm() {
    this.formGroup = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }

  loginProcess() {
    const val = this.formGroup.value;
    if (this.formGroup.valid) {
      this.showForm = false;
      this.service.login(val.username, val.password).subscribe(
        (res: any) => {
          if (res.user.is_active) {
            if (res.user.confirmed_account) {
              localStorage.setItem('token', res.token);
              localStorage.setItem('user', JSON.stringify(res.user));
              localStorage.setItem('userId', JSON.stringify(res.user.id));

              localStorage.setItem('navItems', JSON.stringify(navItems));
              localStorage.setItem('title', 'Initialisation');
              localStorage.setItem('lastNav', '1');
              this.router.navigate(['/home']);
            }
          } else {
            this.showForm = true;
            this.error =
              "Votre compte n'est pas activé. Veuillez contactez votre administrateur pour obtenir plus d'informations.";
          }
        },
        (error) => {
          if (error.status == 401) {
            this.router.navigate(['/login']);
            this.showForm = true;
            this.error = 'Le mail ou mot de passe est incorrect !';
          }
          if (error.status == 504) {
            this.router.navigate(['/login']);
            this.showForm = true;
            this.error = 'Vérifier votre connexion internet et réessayer';
          } else {
            this.showForm = true;
            this.error = 'Le mail ou mot de passe est incorect !';
          }
        }
      );
    } else {
      this.error = 'Mail ou mot de passe manquant';
      console.log(this.error);
    }
  }
}
