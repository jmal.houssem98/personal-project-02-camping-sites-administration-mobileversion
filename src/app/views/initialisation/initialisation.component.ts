import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {DataService} from "../../shared/services/data.service";
import {Router} from "@angular/router";
import {Observable, Subject} from "rxjs";
import {WebcamImage} from "ngx-webcam";

@Component({
  selector: 'app-initialisation',
  templateUrl: './initialisation.component.html',

  styleUrls: [
    '../../../../node_modules/simple-line-icons/css/simple-line-icons.css',
    './initialisation.component.css',
  ],
})
export class InitialisationComponent implements OnInit {
  newElements=[]
  stream: any = null;
  sites=[]
  places=[]
  versions=[]
  accommodations=[]
  elements=[]
  canvasProviders=[]
  manufacturers=[]
  step=1;
  scanResult: any = '';
  enable = false;
  noElement=false
  devices: any;
  deviceActif: any;
  errorCam = false;
  showelementAtt = false;
  site="";
  place="";
  version="";
  accommodation="";
  accommodationgeted= {
    leasing:'',
    place: {name:'',
      site:{name:''}}
  };
  element='';
  manufacturer="";
  canvasProvider="";
  loading= false;
  errorElement= false;
  selectedElement: any;
  exist=false;
  constructor(
    private share: DataService,
    private router: Router,
  ) {}

  ngOnInit(): void {
  }

  scanQrCode() {
    this.enable = true;
  }
  onCodeResult(result: string) {
    this.scanResult = result;
    this.enable = false;
  }

  sdevices(x: any) {
    console.log('dddd', x);
    this.devices = x;
  }

  notFoundCam() {
    console.log('not found cam');
    this.errorCam = true;
  }

  closeCam() {
    this.enable = false;
    this.errorCam = false;
  }

  getSites() {
    this.loading=true
    this.showelementAtt=false
    this.site=""
    this.place="";
    this.version="";
    this.accommodation="";
    this.element="";
    this.manufacturer="";
    this.canvasProvider="";
    this.sites=[]
    this.manufacturers=[]
    this.canvasProviders=[]
    this.elements=[]
    this.accommodations=[]
    this.versions=[]
    this.places=[]
    this.loading=true
    this.share
      .getEntity("/sites", [{ is_active: "true" }])
      .subscribe((result) => {
        console.log(result)
        // @ts-ignore
        this.sites = result["hydra:member"];
        this.loading=false
      },error => {
        if (error.status == 401) {
          localStorage.clear()
          this.router.navigate(["/login"]);
        }
        console.log(error)
      });
  }

  show() {
    console.log(this.site)
  }
  getVersions() {
    this.noElement=false
    this.versions=[]
    this.loading=true
    this.place=""
    this.version="";
    this.showelementAtt=false
    this.accommodation="";
    this.element="";
    this.manufacturer="";
    this.canvasProvider="";
    this.manufacturers=[]
    this.canvasProviders=[]
    this.elements=[]
    this.accommodations=[]
    this.places=[]
    this.share
      .getEntity("/versions/not_initialized?siteId="+this.site, [])
      .subscribe((result) => {
        // @ts-ignore
        if (result["hydra:member"].length===0){
          this.noElement=true
        }else {
          // @ts-ignore
          this.versions = result["hydra:member"];
        }
        this.loading=false
      },error => {
        if (error.status == 401) {
          localStorage.clear()
          this.router.navigate(["/login"]);
        }
        console.log(error)
      });
  }
  getPlaces() {
    this.loading=true
    this.showelementAtt=false
    this.place="";
    this.accommodation="";
    this.element="";
    this.manufacturer="";
    this.canvasProvider="";
    this.manufacturers=[]
    this.canvasProviders=[]
    this.elements=[]
    this.accommodations=[]
    this.share
      .getEntity("/places/not_initialized?siteId="+this.site+"&versionId="+this.version, [])
      .subscribe((result) => {
        console.log(result)
        // @ts-ignore
        this.places = result["hydra:member"];
        this.loading=false
      },error => {
        if (error.status == 401) {
          localStorage.clear()
          this.router.navigate(["/login"]);
        }
        console.log(error)
      });
  }
  getAccommodations(){
    this.loading=true
    this.showelementAtt=false
    this.accommodation="";
    this.element="";
    this.manufacturer="";
    this.canvasProvider="";
    this.manufacturers=[]
    this.canvasProviders=[]
    this.elements=[]
    this.getAttribut()
    this.share
      .getEntity("/accommodations", [{version:this.version},{place:this.place}
      ])
      .subscribe((result) => {
        // @ts-ignore
        this.elements = result["hydra:member"][0]['elements'].filter(x=>x.recognition_code==  '');
        console.log(this.elements)
        this.loading=false
      },error => {
        if (error.status == 401) {
          localStorage.clear()
          this.router.navigate(["/login"]);
        }
        console.log(error)
      });
  }

  getElement() {
    this.loading=true
    this.errorElement=false
    this.loading=true
    this.showelementAtt=false
    this.element="";
    this.manufacturer="";
    this.canvasProvider="";
    this.manufacturers=[]
    this.canvasProviders=[]
    this.getAttribut()
    this.share
      .getEntity("/elements", [{accommodation:this.accommodation},{recognition_code:""}])
      .subscribe((result) => {
        console.log(result)
        this.loading=false
        // @ts-ignore
        this.elements = result["hydra:member"];
        if (this.elements.length>0){
          if (this.step!=3){
            this.showelementAtt = true
          }
        }else {
          this.errorElement=true
        }
        this.loading=false
      },error => {
        if (error.status == 401) {
          localStorage.clear()
          this.router.navigate(["/login"]);
        }
        console.log(error)
      });
  }
  getCanvasProvider() {
    this.share.getEntity("/canvas_providers", []).subscribe((result) => {
      // @ts-ignore
      this.canvasProviders = result["hydra:member"];
    },error => {
      if (error.status == 401) {
        localStorage.clear()
        this.router.navigate(["/login"]);
      }
      console.log(error)
    });
  }
  getManufacturer() {
    this.share.getEntity("/manufacturers", []).subscribe((result) => {
      // @ts-ignore
      this.manufacturers = result["hydra:member"];
    },error => {
      if (error.status == 401) {
        localStorage.clear()
        this.router.navigate(["/login"]);
      }
      console.log(error)
    });
  }
  getAttribut() {
    this.manufacturer="";
    this.canvasProvider="";
    this.getCanvasProvider()
    this.getManufacturer()
  }

  initialiser() {
    if (this.scanResult!=''){
      this.loading = true
      let element = {
        name:this.selectedElement,
        recognitionCode: this.scanResult,
        canvasProvider: this.canvasProvider,
        manufacturer: this.manufacturer,
      }
      console.log(this.element)
      this.share
        .updateEntity("/elements", this.element, element)
        .subscribe(
          async (res) => {
            // @ts-ignore
            this.newElements.push(element)
            this.showelementAtt=false;
            console.log(res)
            await this.getACC()
            await this.getElement()
            this.scanResult = ''
            this.element = "";
            this.manufacturer = "";
            this.canvasProvider = "";
            this.previewImage=''
            if (this.elements.length > 1) {
              this.step = 3
            } else {
              this.step = 4
            }
          },error => {
            if (error.status == 401) {
              localStorage.clear()
              this.router.navigate(["/login"]);
            }
            console.log(error)
            this.loading=false
          }
        );
    }else {

    }
  }
  getACC(){
    this.loading=true
    this.share
      .getAccommodation("/api/accommodations/1")
      .subscribe((result) => {
        // @ts-ignore
        this.accommodationgeted=result
        this.loading=false
        console.log(result)
      },error => {
        if (error.status == 401) {
          localStorage.clear()
          this.router.navigate(["/login"]);
        }
        console.log(error)
      });
  }

  terminer() {
    this.step=1
    this.newElements=[]
    this.sites=[]
    this.places=[]
    this.versions=[]
    this.accommodations=[]
    this.elements=[]
    this.canvasProviders=[]
    this.manufacturers=[]
  }
  confirmCode() {
    this.exist = false;
    this.loading=true
    this.share
      .getEntity("/elements", [
        { recognition_code: this.scanResult },
      ])
      .subscribe((res) => {
        this.loading=false
          // @ts-ignore
          if (res["hydra:member"].length > 0) {
            // @ts-ignore
            res["hydra:member"].map((item) => {
              if (item.recognition_code === this.scanResult) {
                this.exist = true;
              }
            });
          } else {
            this.step=2
            this.getSites()
          }
      },error => {
        if (error.status == 401) {
          localStorage.clear()
          this.router.navigate(["/login"]);
        }
        console.log(error)
      });
  }

  terminerInit() {
    this.step=4
  }

  //Camera Functions
  showSuccessDialog = false;
  status: any = null;
  trigger: Subject<void> = new Subject();
  previewImage: string = '';
  get $trigger(): Observable<void> {
    return this.trigger.asObservable();
  }

  snapshot(event: WebcamImage) {
    console.log(event);
    this.previewImage = event.imageAsDataUrl;
  }

  captureImage() {
    console.log('lets take a picture');
    this.trigger.next();
  }
  checkPermissions() {
    navigator.mediaDevices
      .getUserMedia({
        video: {
          width: 500,
          height: 500,
        },
      })
      .then((res) => {
        console.log('response', res);
        this.stream = res;
      })
      .catch((err) => {
        console.log(err);
        this.status = err;
      });
  }
  proceed() {
    //console.log(this.previewImage);
    this.showSuccessDialog = true;
  }

  closeWebCam() {
    //console.log('close camera plz');
    this.stream = null;
  }
}
