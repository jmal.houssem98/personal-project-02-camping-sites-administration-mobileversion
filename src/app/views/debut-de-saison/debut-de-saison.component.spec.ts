import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DebutDeSaisonComponent } from './debut-de-saison.component';

describe('DebutDeSaisonComponent', () => {
  let component: DebutDeSaisonComponent;
  let fixture: ComponentFixture<DebutDeSaisonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DebutDeSaisonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DebutDeSaisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
