import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinDeSaisonComponent } from './fin-de-saison.component';

describe('FinDeSaisonComponent', () => {
  let component: FinDeSaisonComponent;
  let fixture: ComponentFixture<FinDeSaisonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinDeSaisonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinDeSaisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
