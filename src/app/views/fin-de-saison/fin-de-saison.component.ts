import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Router } from '@angular/router';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-fin-de-saison',
  templateUrl: './fin-de-saison.component.html',
  styleUrls: [
    './fin-de-saison.component.css',
    '../../../../node_modules/simple-line-icons/css/simple-line-icons.css',
  ],
})
export class FinDeSaisonComponent implements OnInit {
  enable = false;
  scanResult: any = '';
  devices: any;
  deviceActif: any;
  errorCam = false;
  step = 1;
  exist = true;
  showDialog = false;
  loading = false;
  showWindow = true;
  elementState = [''];
  reparationType: any;
  versionName: any;
  showButton = false;
  showSuccessDialog = false;

  constructor(private share: DataService, private router: Router) {}

  ngOnInit(): void {
    this.previewImage = '';
  }

  scanQrCode() {
    this.enable = true;
  }
  onCodeResult(result: string) {
    this.scanResult = result;
    this.enable = false;
  }

  sdevices(x: any) {
    console.log('dddd', x);
    this.devices = x;
  }

  notFoundCam() {
    console.log('not found cam');
    this.errorCam = true;
  }

  closeCam() {
    this.enable = false;
    this.errorCam = false;
  }

  accommodation: any;
  confirmCode() {
    this.hideSelect = false;
    this.showNextButton = true;
    this.previewImage = '';
    this.accommodation = [];
    this.loading = true;
    this.share
      .getEntity('/elements', [{ recognition_code: this.scanResult }])
      .subscribe((res) => {
        this.loading = false;
        // @ts-ignore
        if (res['hydra:member'].length > 0) {
          // @ts-ignore
          res['hydra:member'].map((item) => {
            if (item.recognition_code === this.scanResult) {
              this.exist = true;
              this.accommodation = item.accommodation;
              this.share
                .getEntity('/versions', [
                  { version: this.accommodation.version },
                ])
                .subscribe(
                  (res) => {
                    // @ts-ignore
                    res['hydra:member'].map((item) => {
                      this.versionName = item.name;
                    });
                    this.loading = false;
                  },

                  (error) => {
                    if (error.status == 401) {
                      localStorage.clear();
                      this.router.navigate(['/login']);
                    }
                    console.log(error);
                  }
                );
            }
          });
        } else {
          this.step = 1;
          this.exist = false;
        }
      });
  }
  //get elements of specific acccomodation
  elements = [];
  getElements() {
    this.elements = [];
    this.share
      .getEntity('/elements', [{ accommodation: this.accommodation.id }])
      .subscribe(
        (result) => {
          // @ts-ignore
          this.elements = result['hydra:member'];
        },
        (error) => {
          if (error.status == 401) {
            localStorage.clear();
            this.router.navigate(['/login']);
          }
          console.log(error);
        }
      );
  }

  elementStates = [];
  getElementStates() {
    this.elements = [];
    this.share.getEntity('/element_states', [{ is_active: 'true' }]).subscribe(
      (result) => {
        // @ts-ignore
        this.elementStates = result['hydra:member'];
        this.loading = false;
      },
      (error) => {
        if (error.status == 401) {
          localStorage.clear();
          this.router.navigate(['/login']);
        }
        console.log(error);
      }
    );
  }

  //functions to control steps
  changeStep(nextStep: number) {
    this.step = nextStep;
  }

  forwardStep() {
    this.step = 2;
    this.getElements();
    this.getElementStates();
  }

  newReparation = {
    amount: 0,
    comment: '',
    sinister: false,
    element: '/api/elements/',
    serviceProvider: 'service Provider',
    reparationSendingDate: '2022-05-21',
    reparationEndDate: '2022-05-21',
    reparationType: '/api/reparation_types/',
  };

  changeReparation() {
    (this.newReparation.element = '/api/elements/'),
      (this.newReparation.reparationType = '/api/reparation_types/'),
      (this.newReparation.comment = this.description);
    this.newReparation.reparationType += String(this.reparationType);
    this.newReparation.element += String(this.currentElementId);

    this.share
      .addEntity('/reparations', this.newReparation)
      .subscribe((res) => {});
  }

  newElementState = {
    element: '/api/elements/',
    createdAt: '2022-05-21',
    updatedAt: '2022-05-21',
    user: '/api/users/1',
    newState: '',
  };

  statut = {
    sinister: false,
    state: '/api/element_states/3',
  };
  description = '';
  price = 0;
  currentElementName = '';
  currentElementId = 0;
  userId: any;
  checkPick(
    x: number,
    myCurrentElementId: number,
    myCurrentElementName: string
  ) {
    this.userId = localStorage.getItem('userId');
    //console.log('user', this.userId);
    this.newElementState.element = '/api/elements/';
    this.newElementState.createdAt = '';
    this.newElementState.newState = '';
    this.newElementState.user = '/api/users/' + String(this.userId);

    let now = new Date();
    console.log(now.toISOString());
    //monté
    this.currentElementName = myCurrentElementName;
    this.currentElementId = myCurrentElementId;
    if (this.elementState[x] == '1') {
      this.newElementState.element += String(this.currentElementId);
      this.newElementState.createdAt = now.toISOString();
      this.newElementState.newState = '/api/element_states/1';
      console.log('monté patching');
      this.share
        .patchEntity('/elements', String(this.currentElementId), {
          state: '/api/element_states/1',
        })
        .subscribe((res) => {});
      this.share
        .addEntity('/element_histories', this.newElementState)
        .subscribe((res) => {});
    }
    //en stock
    this.currentElementName = myCurrentElementName;
    if (this.elementState[x] == '4') {
      this.newElementState.element += String(this.currentElementId);
      this.newElementState.createdAt = now.toISOString();
      this.newElementState.newState = '/api/element_states/4';
      console.log('en stock patching');
      this.share
        .patchEntity('/elements', String(this.currentElementId), {
          state: '/api/element_states/4',
        })
        .subscribe((res) => {});
      this.share
        .addEntity('/element_histories', this.newElementState)
        .subscribe((res) => {});
    }

    this.price = 0;
    if (this.elementState[x] == '2') {
      this.loading = true;
      this.step = 3;
      this.share
        .patchEntity('/elements', String(this.currentElementId), {
          state: '/api/element_states/2',
        })
        .subscribe((res) => {});
      this.share
        .getEntity('/pricings', [{ element_type: myCurrentElementId }])
        .subscribe(
          (res) => {
            // @ts-ignore
            res['hydra:member'].map((item) => {
              this.price = item.cost;
            });
            this.loading = false;
          },
          (error) => {
            if (error.status == 401) {
              localStorage.clear();
              this.router.navigate(['/login']);
            }
            console.log(error);
          }
        );

      this.newElementState.element += String(this.currentElementId);
      this.newElementState.createdAt = now.toISOString();
      this.newElementState.newState = '/api/element_states/2';

      this.share
        .addEntity('/element_histories', this.newElementState)
        .subscribe((res) => {});
    }
    if (this.elementState[x] == '3') {
      this.nextPart = false;
      this.showButtons = false;
      this.loading = true;
      this.step = 4;

      this.newElementState.element += String(this.currentElementId);
      this.newElementState.createdAt = now.toISOString();
      this.newElementState.newState = '/api/element_states/3';

      this.share
        .addEntity('/element_histories', this.newElementState)
        .subscribe((res) => {});
    }
    this.loading = false;
  }

  nextPart = false;
  showButtons = false;
  showNext() {
    this.nextPart = true;
    this.showButtons = true;
  }
  hideSelect = false;
  showNextButton = true;
  updateElementsStatus() {
    this.hideSelect = true;
    this.showNextButton = false;
  }
  changeSinister(bool: boolean) {
    this.statut.sinister = bool;

    this.share
      .patchEntity('/elements', String(this.currentElementId), this.statut)
      .subscribe((res) => {});
  }

  //Camera Functions
  stream: any = null;
  status: any = null;
  trigger: Subject<void> = new Subject();
  previewImage: string = '';
  get $trigger(): Observable<void> {
    return this.trigger.asObservable();
  }

  snapshot(event: WebcamImage) {
    console.log(event);
    this.previewImage = event.imageAsDataUrl;
  }

  captureImage() {
    console.log('lets take a picture');
    this.trigger.next();
  }

  captureImage2() {
    console.log('lets take a picture');
    this.trigger.next();
  }

  checkPermissions() {
    navigator.mediaDevices
      .getUserMedia({
        video: {
          width: 500,
          height: 500,
        },
      })
      .then((res) => {
        console.log('response', res);
        this.stream = res;
      })
      .catch((err) => {
        console.log(err);
        this.status = err;
      });
  }
  proceed() {
    //console.log(this.previewImage);
    this.showSuccessDialog = true;
  }

  closeWebCam() {
    //console.log('close camera plz');
    this.stream = null;
  }
}
