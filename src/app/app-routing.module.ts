import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/authentication/auth.guard';

import { LoginComponent } from './views/login/login.component';
import { DefaultLayoutComponent } from './containers/default-layout/default-layout.component';
import { InitialisationComponent } from './views/initialisation/initialisation.component';
import { MiseEnRebutComponent } from './views/mise-en-rebut/mise-en-rebut.component';
import { ReportingComponent } from './views/reporting/reporting.component';
import { FinDeSaisonComponent } from './views/fin-de-saison/fin-de-saison.component';
import { DebutDeSaisonComponent } from './views/debut-de-saison/debut-de-saison.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  // { path: 'home', component: HomeComponent },
  {
    path: 'home',
    component: DefaultLayoutComponent,
    children: [
      { path: '', redirectTo: '/home/initialisation', pathMatch: 'full' },
      {
        path: 'initialisation',
        component: InitialisationComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'mise-en-rebut',
        component: MiseEnRebutComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'reporting',
        component: ReportingComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'fin-de-saison',
        component: FinDeSaisonComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'debut-de-saison',
        component: DebutDeSaisonComponent,
        canActivate: [AuthGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
