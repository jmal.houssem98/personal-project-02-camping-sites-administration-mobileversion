export const navItems = [
  {
    name: "Fin de Saison",
    url: "home/fin-de-saison",
    icon: "icon-power",
    active:"",
  },
  {
    name: "Initialisation",
    url: "home/initialisation",
    icon: "icon-pencil",
    active:"activeNav",
  },
  {
    name: "Mise en Rebut",
    url: "home/mise-en-rebut",
    icon: "icon-present",
    active:"",
  },
  {
    name: "Reporting",
    url: "home/reporting",
    icon: "icon-chart",
    active:"",
  },
  // {
  //   name: "Debut de Saison",
  //   url: "home/debut-de-saison",
  //   icon: "icon-present",
  //   active:"",
  // },
];
